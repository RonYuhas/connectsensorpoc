#include "bluefruit.h"
#include "BLEFitService.h"

BLEFitService::BLEFitService(void) :
	BLEService(0x181C), _angle(0xA001), _distance(0xA010)
{

}

err_t BLEFitService::begin(void)
{
	// Invoke base class begin()
	VERIFY_STATUS(BLEService::begin());

	_angle.setProperties(CHR_PROPS_READ | CHR_PROPS_NOTIFY); // could support notify
	_angle.setPermission(SECMODE_OPEN, SECMODE_NO_ACCESS);
	//_angle.setFixedLen(1);
	VERIFY_STATUS(_angle.begin());

	_distance.setProperties(CHR_PROPS_READ | CHR_PROPS_NOTIFY); // could support notify
	_distance.setPermission(SECMODE_OPEN, SECMODE_NO_ACCESS);
	VERIFY_STATUS(_distance.begin());

	return ERROR_NONE;
}

bool BLEFitService::writeAngle(uint16_t level)
{
	return _angle.write8(level) > 0;
}

bool BLEFitService::notifyAngle(uint16_t level)
{
	return _angle.notify16(level);
}

bool BLEFitService::notifyAngle(const char* str)
{
	return _angle.notify(str);
}

bool BLEFitService::writeDistance(uint16_t level)
{
	return _distance.write8(level) > 0;
}

bool BLEFitService::notifyDistance(uint16_t level)
{
	return _distance.notify16(level);
}

bool BLEFitService::notifyDistance(const char* str)
{
	return _distance.notify(str);
}