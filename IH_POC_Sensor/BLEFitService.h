#ifndef BLEFITS_H_
#define BLEFITS_H_

#include "bluefruit_common.h"

#include "BLECharacteristic.h"
#include "BLEService.h"

class BLEFitService : public BLEService
{
protected:
	BLECharacteristic _angle;
	BLECharacteristic _distance;
public:
	BLEFitService(void);

	virtual err_t begin(void);

	bool writeAngle(uint16_t level);
	bool notifyAngle(uint16_t level);
	bool notifyAngle(const char* str);

	bool writeDistance(uint16_t level);
	bool notifyDistance(uint16_t level);
	bool notifyDistance(const char* str);
};



#endif /* BLEBAS_H_ */